package cat.itb.m05.uf2;

public class Notes {

    //TODO: Crea una branca nova "testing_documentacio_refactoring" i fes les següents modificacions en ella:

    //TODO: Fer javaDoc de la classe i metodes/funcions

    //TODO: Generar test amb JUnit5, per comprobar si funcionen correctament els metodes/funcions d'aquesta classe

    //TODO: Aplicar les tècniques de refactoring, no cal modificar el funcionament.

    //TODO: En acabar, incorpora els canvis de la branca màster, i puja-ho tot a un projecte privat de gitlab, invitant a @marc.vives.

    public static int funcio32(double a){ //
        //si el numero es més gran o igual a 5, fa el arrodoniment, si es mes petit que 4 també.
        //qualsevol numero entre 4 i 5 serà un 5
        if(a<=5 && a>4){
            return 4;
        }else{
            return (int) Math.round(a);
        }
    }

    public static int funcio24(int a, double b){
        //si b és més gran o igual a 20, i la nota és més gran que 5, retorna un 4
        //si b és més gran o igual a 20, i la nota és més petita que 5, retorna la nota
        //en qualsevol altre cas retorna la nota.
        return 4;
    }
}
